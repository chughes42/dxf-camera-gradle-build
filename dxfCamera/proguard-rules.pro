

-dontwarn android.support.v7.**
-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }

-dontwarn org.opencv.**
-keep class org.opencv** { *; }


-dontwarn com.flurry.**
-keep class com.flurry.** { *; }


