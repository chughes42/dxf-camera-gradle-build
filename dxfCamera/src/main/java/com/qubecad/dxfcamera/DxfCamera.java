package com.qubecad.dxfcamera;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Toast;


import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import com.github.clans.fab.FloatingActionButton;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import static android.support.v4.content.FileProvider.getUriForFile;

public class DxfCamera extends Activity
        implements CvCameraViewListener, ActivityCompat.OnRequestPermissionsResultCallback {
    private static final String TAG = "DxfCamera";


    private static final int PERMISSIONS_REQUEST_CAMERA = 0;
    private static final int PERMISSIONS_REQUEST_WRITE_SDCARD = 1;

    private Mat mRgba;
    private Mat mIntermediateMat;
    private SeekBar sb;
    private CameraView mOpenCvCameraView;
    private String fileNameDXF = "";
    private String capturedFilename = "";
    private Uri filepathImage;
    private boolean proImage = false;
    private Activity activity = this;
    private Bitmap finalBmp;
    private int cameraId;
    private Mat mRotRgba;
    private SharedPreferences sharedPref;
    private String fileNamePrefix;
    private boolean smoothingEnabled = false;
    private boolean usePoints = false;
    private Context context;

    private String layerColour = "7";

    private boolean rotateScreen;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");

                    mOpenCvCameraView.enableView();

                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    public DxfCamera() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        context = this;
        // Get intent, action and MIME type
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        Log.i(TAG, action);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        layerColour = sharedPref.getString(SettingsActivity.KEY_DXF_LINE_COLOUR, "7");
        fileNamePrefix = sharedPref.getString(SettingsActivity.KEY_DXF_FILENAME_PREFIX, "Dxf_Camera_output");

        if ("White".equals(layerColour)) {
            layerColour = "7";
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(SettingsActivity.KEY_DXF_LINE_COLOUR, "7");
            editor.commit();
        }


        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        setContentView(R.layout.surface_view);

        sb = (SeekBar) findViewById(R.id.seekBar1);


        sb.setMax(500);
        sb.setProgress(250);

        rotateScreen = "NEXUS 5X".equalsIgnoreCase(Build.MODEL);

        ImageButton captureButton = (ImageButton) findViewById(R.id.imageButton1);

        captureButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                // startCaptureImageTask();
                checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSIONS_REQUEST_WRITE_SDCARD);
            }

        });

        FloatingActionButton openButton = (FloatingActionButton) findViewById(R.id.imageButton2);

        openButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

                if (!capturedFilename.equals("")) {
                    openDxf(capturedFilename);
                } else {
                    Toast.makeText(activity, getString(R.string.no_dxf), Toast.LENGTH_LONG).show();
                }
            }

        });

        FloatingActionButton infoButton = (FloatingActionButton) findViewById(R.id.imageButton3);

        infoButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                // startCaptureImageTask();
                about_dialog();
            }

        });

        FloatingActionButton settingsButton = (FloatingActionButton) findViewById(R.id.imageButton4);

        settingsButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                // startCaptureImageTask();
                showSettings();

            }

        });


        mOpenCvCameraView = (CameraView) findViewById(R.id.surface_view);
        mOpenCvCameraView.setCvCameraViewListener(this);

        cameraId = mOpenCvCameraView.getCameraID();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void showSettings() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }


    private void checkPermission(String permission, int callbackRequestId) {
        int permissionCheck = ContextCompat.checkSelfPermission(this, permission);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {

                Toast.makeText(DxfCamera.this, "Please grant access for operation to proceed.", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(this, new String[]{permission}, callbackRequestId);

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this, new String[]{permission}, callbackRequestId);

                // The callback method gets the
                // result of the request.
            }
        } else {
            switch (callbackRequestId) {

                case PERMISSIONS_REQUEST_CAMERA: {

                    OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);

                    return;

                }
                case PERMISSIONS_REQUEST_WRITE_SDCARD: {

                    startCaptureImageTask();

                }
                return;

            }

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();

    }

    @Override
    public void onResume() {
        super.onResume();
        checkPermission(Manifest.permission.CAMERA, PERMISSIONS_REQUEST_CAMERA);
        context = this;
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mRotRgba = new Mat(height, width, CvType.CV_8UC4);
        mIntermediateMat = new Mat(height, width, CvType.CV_8UC4);


    }

    public void onCameraViewStopped() {
        mRgba.release();
        mIntermediateMat.release();
    }

    public Mat onCameraFrame(Mat inputFrame) {


        if (!proImage) {


            Imgproc.Canny(inputFrame, mIntermediateMat, sb.getProgress(), sb.getProgress());
            Imgproc.cvtColor(mIntermediateMat, mRgba, Imgproc.COLOR_GRAY2BGRA, 4);


            if (rotateScreen) {
                Core.flip(mRgba, mRotRgba, -1);
            } else {
                mRotRgba = mRgba;
            }


        }


        return mRotRgba;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "called onCreateOptionsMenu");
        new MenuInflater(this).inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
        switch (item.getItemId()) {

            case R.id.CAPTURE_ID:
                checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSIONS_REQUEST_WRITE_SDCARD);
                return true;

            case R.id.OPEN_ID:
                if (!fileNameDXF.equals("")) {
                    openDxf(fileNameDXF);
                } else {
                    Toast.makeText(this, getString(R.string.no_dxf), Toast.LENGTH_LONG).show();
                }
                return true;

            case R.id.ABOUT_ID:
                about_dialog();
                return true;

        }
        return (super.onOptionsItemSelected(item));
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();

        switch (keyCode) {

            case KeyEvent.KEYCODE_VOLUME_UP:

                if (action == KeyEvent.ACTION_UP) {
                    checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSIONS_REQUEST_WRITE_SDCARD);

                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_DOWN) {
                    // save point

                }
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }

    }

    /**
     * Set up and execute an Async Task to create the DXf file
     */
    private void startCaptureImageTask() {
        if (!proImage) {
            proImage = true;
            // capture point

            Mat tempMatLines = new Mat();
            tempMatLines = mIntermediateMat.clone();
            captureImage task = new captureImage();
            task.execute(tempMatLines);

        }
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("DxfCamera Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    /**
     *
     */

    private class captureImage extends AsyncTask<Mat, Void, Uri> {

        protected Uri doInBackground(Mat... unit) {

            layerColour = sharedPref.getString(SettingsActivity.KEY_DXF_LINE_COLOUR, "7");
            fileNamePrefix = sharedPref.getString(SettingsActivity.KEY_DXF_FILENAME_PREFIX, "Dxf_Camera_output");
            smoothingEnabled = sharedPref.getBoolean(SettingsActivity.KEY_DXF_ENABLE_SMOOTHING, false);
            usePoints = sharedPref.getBoolean(SettingsActivity.KEY_DXF_USE_POINTS, false);
            String fileNameFront = fileNamePrefix + "-" + MiscUtils.now();
            fileNameDXF = fileNameFront + ".dxf";
            String fileNameImage = fileNameFront + ".png";

            File path = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/dxfcamera/images/");
            path.mkdirs();
            File fileResultImage = new File(path, fileNameImage);

            filepathImage = Uri.parse(fileResultImage.toString());
            Log.i(TAG, fileResultImage.toString());

            finalBmp = BitmapHelper.getBitmap(unit[0]);

            List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
            Imgproc.findContours(unit[0], contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
            MatOfPoint approxf1 = new MatOfPoint();
            MatOfPoint2f points2f;
            MatOfPoint2f points2fOut;

            DxfTools dxf = new DxfTools(fileNameDXF);
            dxf.setLayerColour(layerColour);
            DxfEntity entity = null;
            Point[] vec;

            double epsilon;
            for (MatOfPoint mop : contours) {

                if (smoothingEnabled) {
                    points2f = new MatOfPoint2f(mop.toArray());
                    points2fOut = new MatOfPoint2f();
                    epsilon = 0.1 * Imgproc.arcLength(points2f, false);
                    Imgproc.approxPolyDP(points2f, points2fOut, epsilon, false);

                    points2fOut.convertTo(approxf1, CvType.CV_32S);

                    vec = approxf1.toArray();

                } else {
                    vec = mop.toArray();
                }

                if (usePoints) {
                    entity = new DxfPoints(vec, layerColour);
                } else {
                    entity = new PolyLine(vec, layerColour);
                }

                dxf.addentity(entity.getDxfEntityCode());
                entity = null;
                vec = null;

            }

            dxf.close();
            Uri resultFile = dxf.getFilename();

            MediaScannerConnection.scanFile(
                    getApplicationContext(),
                    new String[]{resultFile.getPath(),} , null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            Log.d(TAG,"Scan media complete");
                        }
                    });


            dxf = null;

            return resultFile;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // create the dialog and attach it to the fragment manager
            ProgressDialogFragment dialog = new ProgressDialogFragment();

            dialog.show(getFragmentManager(), ProgressDialogFragment.PROGRESS_DIALOG_TAG);

        }

        protected void onPostExecute(Uri filename) {
            ProgressDialogFragment dialog = (ProgressDialogFragment) getFragmentManager()
                    .findFragmentByTag(ProgressDialogFragment.PROGRESS_DIALOG_TAG);
            if (dialog != null) {
                dialog.dismiss();
            }

            proImage = false;
            capturedFilename = filename.toString();
            Toast.makeText(activity, getString(R.string.dxf_exported_to) + " " + capturedFilename, Toast.LENGTH_LONG).show();

            if (getIntent().getAction() != MediaStore.ACTION_IMAGE_CAPTURE) {
                IntentHelper.sendfile(filename.toString(), activity);
            } else {

                Uri saveUri = getIntent().getExtras().getParcelable(MediaStore.EXTRA_OUTPUT);

                if (saveUri != null) {
                    OutputStream imageOutputStream;
                    byte[] bmpByteArray = BitmapHelper.convertBitmaptoByteArray(finalBmp);

                    try {
                        imageOutputStream = getContentResolver().openOutputStream(saveUri);
                        imageOutputStream.write(bmpByteArray);
                        imageOutputStream.flush();
                        imageOutputStream.close();
                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                Intent resultIntent = new Intent();

                resultIntent.setData(filepathImage);
                resultIntent.putExtra("dxffile", filename.toString());
                setResult(Activity.RESULT_OK, resultIntent);


                finish();
            }
        }

    }


    /**
     * Saves the Mat to file. On success returns the passed path or Null if the
     * save failed.
     *
     * @param mat
     * @param filename
     * @return
     */

    public String SaveImage(Mat mat, File filename) {
        Mat mIntermediateMat = new Mat();
        // Imgproc.cvtColor(mat, mIntermediateMat, Imgproc.COLOR_RGBA2BGR, 3);

        String file = filename.toString();
        Log.i(TAG + "SaveImage()", file);
        Boolean bool = Imgcodecs.imwrite(file, mat);

        if (bool) {
            Log.i(TAG, "SUCCESS writing image to external storage");

            try {
                MediaStore.Images.Media.insertImage(getContentResolver(), filename.getAbsolutePath(),
                        filename.getName(), filename.getName());
            } catch

                    (FileNotFoundException e) {
                e.printStackTrace();
            }

        } else {
            Log.i(TAG, "Fail writing image to external storage");
            file = null;
        }

        return file;
    }


    private void openDxf(String filename) {
        if (filename != "") {


            Log.d("Open Dxf", filename);
            try {
               // File path = new File(getExternalFilesDir(null) + "/images/");
                //filename = path.getAbsolutePath() + "/images/" + filename;
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                File file = new File(filename);

               Uri contentUri = getUriForFile(this, "com.qubecad.dxfcamera.fileprovider", file);

                //Uri contentUri =Uri.fromFile(file);
                intent.setDataAndType(contentUri, "image/vnd.dxf");
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Log.e(TAG, "", e);
            }

        } else {

        }

    }


    @Override
    protected void onStart() {
        super.onStart();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    protected void onStop() {
        super.onStop();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.disconnect();
    }

    /**
     * Get the Application version from the package manager
     */
    public String get_ver() {
        String APP_VER = "";
        try {
            PackageManager pm = getPackageManager();
            PackageInfo packageInfo = pm.getPackageInfo(this.getPackageName(), 0);
            APP_VER = (packageInfo.versionName + "." + packageInfo.versionCode);

        } catch (NameNotFoundException e) {

            e.printStackTrace();
        }

        return APP_VER;
    }

    private void about_dialog() {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("About");
        alert.setMessage("Dxf Camera Release " + get_ver()
                + "\r\n" + getResources().getString(R.string.privacy_info));
        alert.show();

    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {

            case PERMISSIONS_REQUEST_CAMERA: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
                } else {
                    Log.d(TAG, "Denied permission Camera" + PERMISSIONS_REQUEST_CAMERA);
                }
                return;

            }
            case PERMISSIONS_REQUEST_WRITE_SDCARD: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //startCaptureImageTask();

                } else {
                    Log.d(TAG, "Denied permission write to SD CARD" + PERMISSIONS_REQUEST_WRITE_SDCARD);
                }
                return;

            }

        }

    }

}
