package com.qubecad.dxfcamera;

import org.opencv.core.Point;

/**
 * Created by carl on 03/02/2017.
 */

public class DxfPoints implements DxfEntity {

    private Point[] points;
    private int Layer=0;
    private String colour;


    DxfPoints(Point[] pointsin, String colour){
        this.points=pointsin;
        this.colour=colour;
    }



    @Override
    public String getDxfEntityCode() {

        StringBuilder pointEntity=new StringBuilder();

        for (Point pnt :points ){
            pointEntity.append("0\r\nPOINT\r\n8\r\nPoints\r\n10\r\n").append(String.valueOf(pnt.x)).append("\r\n20\r\n").append(String.valueOf(pnt.y)).append("\r\n30\r\n0\r\n62\r\n").append(colour).append("\r\n");
        }


        return pointEntity.toString();
    }
}


