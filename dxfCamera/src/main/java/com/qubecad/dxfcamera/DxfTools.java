package com.qubecad.dxfcamera;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;

public class DxfTools {
	
	private static final String TAG = "DxfTools";
	private Uri filename;

	public static void createdxf(Bitmap bmp) {




		try {
			int width=bmp.getWidth();
			int height=bmp.getHeight();
			int value=0;
			int strptx=0;
			int strpty=0;
			int endptx=0;
			int endpty=0;
			int i=1;
			File sdDir = new File(Environment.getExternalStorageDirectory().getPath());
			String filename="output.dxf";
			FileOutputStream fout;

			fout = new FileOutputStream (sdDir.getAbsolutePath()+"/"+filename);
			filename=sdDir.getAbsolutePath()+"/"+filename;

			Writer f = new OutputStreamWriter(fout,"8859_1");
			f.write("  0\r\nSECTION\r\n  2\r\nHEADER\r\n  9\r\n$ACADVER\r\n  1\r\nAC1009\r\n  9\r\n$DIMASZ\r\n 40\r\n2.5\r\n  9\r\n$DIMGAP\r\n 40\r\n0.625\r\n  9\r\n$DIMEXO\r\n 40\r\n0.625\r\n  9\r\n$DIMTXT\r\n 40\r\n2.5\r\n  9\r\n$PLIMMAX\r\n 10\r\n210.0\r\n 20\r\n297.0\r\n  9\r\n$PLIMMIN\r\n 10\r\n0.0\r\n 20\r\n0.0\r\n  9\r\n$DIMEXE\r\n 40\r\n1.25\r\n  0\r\nENDSEC\r\n");// write DXF R12 header
			f.write("  0\r\nSECTION\r\n  2\r\nTABLES\r\n  0\r\nTABLE\r\n  2\r\nVPORT\r\n 70\r\n1\r\n  0\r\nVPORT\r\n  2\r\n*Active\r\n 70\r\n0\r\n 10\r\n0.0\r\n 20\r\n0.0\r\n 11\r\n1.0\r\n 21\r\n1.0\r\n 12\r\n286.30555555555549\r\n 22\r\n148.5\r\n 13\r\n0.0\r\n 23\r\n0.0\r\n 14\r\n10.0\r\n 24\r\n10.0\r\n 15\r\n10.0\r\n 25\r\n10.0\r\n 16\r\n0.0\r\n 26\r\n0.0\r\n 36\r\n1.0\r\n 17\r\n0.0\r\n 27\r\n0.0\r\n 37\r\n0.0\r\n 40\r\n297.0\r\n 41\r\n1.92798353909465\r\n 42\r\n50.0\r\n 43\r\n0.0\r\n 44\r\n0.0\r\n 50\r\n0.0\r\n 51\r\n0.0\r\n 71\r\n0\r\n 72\r\n100\r\n 73\r\n1\r\n 74\r\n3\r\n 75\r\n1\r\n 76\r\n1\r\n 77\r\n0\r\n 78\r\n0\r\n  0\r\nENDTAB\r\n"); //write viewport
			f.write("  0\r\nTABLE\r\n  2\r\nLTYPE\r\n 70\r\n19\r\n  0\r\nLTYPE\r\n  2\r\nCONTINUOUS\r\n 70\r\n0\r\n  3\r\nSolid line\r\n 72\r\n65\r\n 73\r\n0\r\n 40\r\n0.0\r\n  0\r\nENDTAB\r\n"); //write linetypes
			f.write("  0\r\nTABLE\r\n  2\r\nLAYER\r\n 70\r\n1\r\n  0\r\nLAYER\r\n  2\r\n0\r\n 70\r\n0\r\n 62\r\n7\r\n  6\r\nCONTINUOUS\r\n  0\r\nENDTAB\r\n"); //write Layer
			f.write("  0\r\nTABLE\r\n  2\r\nSTYLE\r\n 70\r\n1\r\n  0\r\nSTYLE\r\n  2\r\nStandard\r\n 70\r\n0\r\n 40\r\n0.0\r\n 41\r\n0.75\r\n 50\r\n0.0\r\n 71\r\n0\r\n 42\r\n2.5\r\n  3\r\ntxt\r\n  4\r\n\r\n  0\r\nENDTAB\r\n"); //write Text Style
			f.write("  0\r\nTABLE\r\n  2\r\nVIEW\r\n 70\r\n0\r\n  0\r\nENDTAB\r\n"); //write view
			f.write("  0\r\nTABLE\r\n  2\r\nUCS\r\n 70\r\n0\r\n  0\r\nENDTAB\r\n"); //write UCS
			f.write("  0\r\nTABLE\r\n  2\r\nAPPID\r\n 70\r\n1\r\n  0\r\nAPPID\r\n  2\r\nACAD\r\n 70\r\n0\r\n  0\r\nENDTAB\r\n"); //write APPID
			f.write("  0\r\nTABLE\r\n  2\r\nDIMSTYLE\r\n 70\r\n1\r\n  0\r\nDIMSTYLE\r\n  2\r\nStandard\r\n  3\r\n\r\n  4\r\n\r\n  5\r\n\r\n  6\r\n\r\n  7\r\n\r\n 40\r\n1.0\r\n 41\r\n2.5\r\n 42\r\n0.625\r\n 43\r\n3.75\r\n 44\r\n1.25\r\n 45\r\n0.0\r\n 46\r\n0.0\r\n 47\r\n0.0\r\n 48\r\n0.0\r\n 70\r\n0\r\n 71\r\n0\r\n 72\r\n0\r\n 73\r\n0\r\n 74\r\n0\r\n 75\r\n0\r\n 76\r\n0\r\n 77\r\n1\r\n 78\r\n8\r\n140\r\n2.5\r\n141\r\n2.5\r\n142\r\n0.0\r\n143\r\n0.03937007874016\r\n144\r\n1.0\r\n145\r\n0.0\r\n146\r\n1.0\r\n147\r\n0.625\r\n170\r\n0\r\n171\r\n3\r\n172\r\n1\r\n173\r\n0\r\n174\r\n0\r\n175\r\n0\r\n176\r\n0\r\n177\r\n0\r\n178\r\n0\r\n  0\r\nENDTAB\r\n"); //write Dimstyle

			f.write("  0\r\nENDSEC\r\n"); // end Section
			f.write("  0\r\nSECTION\r\n  2\r\nBLOCKS\r\n  0\r\nENDSEC\r\n"); //write blocks section




			f.write("  0\r\nSECTION\r\n  2\r\nENTITIES\r\n");

			for(int y = 0; y < height; ++y) {
				for(int x = 0; x < width; ++x) {
					value=bmp.getPixel(x,y);


					if (value>=-10000){
						//line?
						strptx=x;
						strpty=y;

						while(bmp.getPixel(x+i, y)>=-10000){
							endptx=x+i;
							endpty=y;
							i++;
							if (x+i>=width) {
								i=0;
								Log.d("createdDxf():","x reset");
								break;
							}
						}

						//if (endptx==0)	{
							try {
								while(bmp.getPixel(x, y+i)>=-10000){
									Log.i("y",String.valueOf(height)+","+String.valueOf(y));
									endptx=x;
									endpty=y+1;
									i++;
									if (y+i>=height) {
										i=0;
										break;
									}
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						//}

						if (endptx!=0)	{

							//create line
							f.write("  0\r\nLINE\r\n  8\r\nPoints\r\n  10\r\n"
									+ String.valueOf(strptx) + "\r\n  20\r\n"
									+ String.valueOf(height-strpty) + "\r\n  30\r\n0\r\n"
									+"  11\r\n"+
									String.valueOf(endptx) + "\r\n  21\r\n"+
									String.valueOf(height-endpty) + "\r\n  31\r\n0\r\n"
									);



						}else

						{	
							//point	
							f.write("  0\r\nPOINT\r\n  8\r\nPoints\r\n  10\r\n"
									+ String.valueOf(x) + "\r\n  20\r\n"
									+ String.valueOf(height-y) + "\r\n  30\r\n0\r\n");	
						}
						strptx=0;
						endptx=0;
						i=0;
					}

				}

			}

			f.write("  0\r\nENDSEC\r\n  0\r\nEOF\r\n");
			f.close();
		} catch (FileNotFoundException e) {

			Log.e(TAG,"CreateDxf()",e);
		} catch (UnsupportedEncodingException e) {

			Log.e(TAG,"CreateDxf()",e);
		} catch (IOException e) {

			Log.e(TAG,"CreateDxf()",e);
		}

	}

	public void setLayerColour(String layerColour) {
		this.layerColour = layerColour;
	}

	private String layerColour="7";
	
	public static void createdxf(List<Line> inputdata) {




		try {
			
			
			
			File sdDir = new File(Environment.getExternalStorageDirectory().getPath());
			String filename="output.dxf";
			FileOutputStream fout;

			fout = new FileOutputStream (sdDir.getAbsolutePath()+"/"+filename);
			filename=sdDir.getAbsolutePath()+"/"+filename;

			Writer f = new OutputStreamWriter(fout,"8859_1");
			f.write("  0\r\nSECTION\r\n  2\r\nHEADER\r\n  9\r\n$ACADVER\r\n  1\r\nAC1009\r\n  9\r\n$DIMASZ\r\n 40\r\n2.5\r\n  9\r\n$DIMGAP\r\n 40\r\n0.625\r\n  9\r\n$DIMEXO\r\n 40\r\n0.625\r\n  9\r\n$DIMTXT\r\n 40\r\n2.5\r\n  9\r\n$PLIMMAX\r\n 10\r\n210.0\r\n 20\r\n297.0\r\n  9\r\n$PLIMMIN\r\n 10\r\n0.0\r\n 20\r\n0.0\r\n  9\r\n$DIMEXE\r\n 40\r\n1.25\r\n  0\r\nENDSEC\r\n");// write DXF R12 header
			f.write("  0\r\nSECTION\r\n  2\r\nTABLES\r\n  0\r\nTABLE\r\n  2\r\nVPORT\r\n 70\r\n1\r\n  0\r\nVPORT\r\n  2\r\n*Active\r\n 70\r\n0\r\n 10\r\n0.0\r\n 20\r\n0.0\r\n 11\r\n1.0\r\n 21\r\n1.0\r\n 12\r\n286.30555555555549\r\n 22\r\n148.5\r\n 13\r\n0.0\r\n 23\r\n0.0\r\n 14\r\n10.0\r\n 24\r\n10.0\r\n 15\r\n10.0\r\n 25\r\n10.0\r\n 16\r\n0.0\r\n 26\r\n0.0\r\n 36\r\n1.0\r\n 17\r\n0.0\r\n 27\r\n0.0\r\n 37\r\n0.0\r\n 40\r\n297.0\r\n 41\r\n1.92798353909465\r\n 42\r\n50.0\r\n 43\r\n0.0\r\n 44\r\n0.0\r\n 50\r\n0.0\r\n 51\r\n0.0\r\n 71\r\n0\r\n 72\r\n100\r\n 73\r\n1\r\n 74\r\n3\r\n 75\r\n1\r\n 76\r\n1\r\n 77\r\n0\r\n 78\r\n0\r\n  0\r\nENDTAB\r\n"); //write viewport
			f.write("  0\r\nTABLE\r\n  2\r\nLTYPE\r\n 70\r\n19\r\n  0\r\nLTYPE\r\n  2\r\nCONTINUOUS\r\n 70\r\n0\r\n  3\r\nSolid line\r\n 72\r\n65\r\n 73\r\n0\r\n 40\r\n0.0\r\n  0\r\nENDTAB\r\n"); //write linetypes
			f.write("  0\r\nTABLE\r\n  2\r\nLAYER\r\n 70\r\n1\r\n  0\r\nLAYER\r\n  2\r\n0\r\n 70\r\n0\r\n 62\r\n7\r\n  6\r\nCONTINUOUS\r\n  0\r\nENDTAB\r\n"); //write Layer
			f.write("  0\r\nTABLE\r\n  2\r\nSTYLE\r\n 70\r\n1\r\n  0\r\nSTYLE\r\n  2\r\nStandard\r\n 70\r\n0\r\n 40\r\n0.0\r\n 41\r\n0.75\r\n 50\r\n0.0\r\n 71\r\n0\r\n 42\r\n2.5\r\n  3\r\ntxt\r\n  4\r\n\r\n  0\r\nENDTAB\r\n"); //write Text Style
			f.write("  0\r\nTABLE\r\n  2\r\nVIEW\r\n 70\r\n0\r\n  0\r\nENDTAB\r\n"); //write view
			f.write("  0\r\nTABLE\r\n  2\r\nUCS\r\n 70\r\n0\r\n  0\r\nENDTAB\r\n"); //write UCS
			f.write("  0\r\nTABLE\r\n  2\r\nAPPID\r\n 70\r\n1\r\n  0\r\nAPPID\r\n  2\r\nACAD\r\n 70\r\n0\r\n  0\r\nENDTAB\r\n"); //write APPID
			f.write("  0\r\nTABLE\r\n  2\r\nDIMSTYLE\r\n 70\r\n1\r\n  0\r\nDIMSTYLE\r\n  2\r\nStandard\r\n  3\r\n\r\n  4\r\n\r\n  5\r\n\r\n  6\r\n\r\n  7\r\n\r\n 40\r\n1.0\r\n 41\r\n2.5\r\n 42\r\n0.625\r\n 43\r\n3.75\r\n 44\r\n1.25\r\n 45\r\n0.0\r\n 46\r\n0.0\r\n 47\r\n0.0\r\n 48\r\n0.0\r\n 70\r\n0\r\n 71\r\n0\r\n 72\r\n0\r\n 73\r\n0\r\n 74\r\n0\r\n 75\r\n0\r\n 76\r\n0\r\n 77\r\n1\r\n 78\r\n8\r\n140\r\n2.5\r\n141\r\n2.5\r\n142\r\n0.0\r\n143\r\n0.03937007874016\r\n144\r\n1.0\r\n145\r\n0.0\r\n146\r\n1.0\r\n147\r\n0.625\r\n170\r\n0\r\n171\r\n3\r\n172\r\n1\r\n173\r\n0\r\n174\r\n0\r\n175\r\n0\r\n176\r\n0\r\n177\r\n0\r\n178\r\n0\r\n  0\r\nENDTAB\r\n"); //write Dimstyle

			f.write("  0\r\nENDSEC\r\n"); // end Section
			f.write("  0\r\nSECTION\r\n  2\r\nBLOCKS\r\n  0\r\nENDSEC\r\n"); //write blocks section




			f.write("  0\r\nSECTION\r\n  2\r\nENTITIES\r\n");
			int i=0;
			for (i=0;i<inputdata.size();i++){
			Line tmpline= inputdata.get(i);
						

							//create line
							f.write("  0\r\nLINE\r\n  8\r\nPoints\r\n  10\r\n"
									+ tmpline.getX1() + "\r\n  20\r\n"
									+ tmpline.getY1() + "\r\n  30\r\n0\r\n"
									+"  11\r\n"+
									tmpline.getX2() + "\r\n  21\r\n"+
									tmpline.getY2() + "\r\n  31\r\n0\r\n"
									);

							tmpline=null;

						

			}

			f.write("  0\r\nENDSEC\r\n  0\r\nEOF\r\n");
			f.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private String DxfHeader(){
		String header="  0\r\nSECTION\r\n  2\r\nHEADER\r\n  9\r\n$ACADVER\r\n  1\r\nAC1009\r\n  9\r\n$DIMASZ\r\n 40\r\n2.5\r\n  9\r\n$DIMGAP\r\n 40\r\n0.625\r\n  9\r\n$DIMEXO\r\n 40\r\n0.625\r\n  9\r\n$DIMTXT\r\n 40\r\n2.5\r\n  9\r\n$PLIMMAX\r\n 10\r\n210.0\r\n 20\r\n297.0\r\n  9\r\n$PLIMMIN\r\n 10\r\n0.0\r\n 20\r\n0.0\r\n  9\r\n$DIMEXE\r\n 40\r\n1.25\r\n  0\r\nENDSEC\r\n"
		+"  0\r\nSECTION\r\n  2\r\nTABLES\r\n  0\r\nTABLE\r\n  2\r\nVPORT\r\n 70\r\n1\r\n  0\r\nVPORT\r\n  2\r\n*Active\r\n 70\r\n0\r\n 10\r\n0.0\r\n 20\r\n0.0\r\n 11\r\n1.0\r\n 21\r\n1.0\r\n 12\r\n286.30555555555549\r\n 22\r\n148.5\r\n 13\r\n0.0\r\n 23\r\n0.0\r\n 14\r\n10.0\r\n 24\r\n10.0\r\n 15\r\n10.0\r\n 25\r\n10.0\r\n 16\r\n0.0\r\n 26\r\n0.0\r\n 36\r\n1.0\r\n 17\r\n0.0\r\n 27\r\n0.0\r\n 37\r\n0.0\r\n 40\r\n297.0\r\n 41\r\n1.92798353909465\r\n 42\r\n50.0\r\n 43\r\n0.0\r\n 44\r\n0.0\r\n 50\r\n0.0\r\n 51\r\n0.0\r\n 71\r\n0\r\n 72\r\n100\r\n 73\r\n1\r\n 74\r\n3\r\n 75\r\n1\r\n 76\r\n1\r\n 77\r\n0\r\n 78\r\n0\r\n  0\r\nENDTAB\r\n"
		+"  0\r\nTABLE\r\n  2\r\nLTYPE\r\n 70\r\n19\r\n  0\r\nLTYPE\r\n  2\r\nCONTINUOUS\r\n 70\r\n0\r\n  3\r\nSolid line\r\n 72\r\n65\r\n 73\r\n0\r\n 40\r\n0.0\r\n  0\r\nENDTAB\r\n"
		+"  0\r\nTABLE\r\n  2\r\nLAYER\r\n 70\r\n1\r\n  0\r\nLAYER\r\n  2\r\n0\r\n 70\r\n0\r\n 62\r\n"+layerColour+"\r\n  6\r\nCONTINUOUS\r\n  0\r\nENDTAB\r\n"
		+"  0\r\nTABLE\r\n  2\r\nSTYLE\r\n 70\r\n1\r\n  0\r\nSTYLE\r\n  2\r\nStandard\r\n 70\r\n0\r\n 40\r\n0.0\r\n 41\r\n0.75\r\n 50\r\n0.0\r\n 71\r\n0\r\n 42\r\n2.5\r\n  3\r\ntxt\r\n  4\r\n\r\n  0\r\nENDTAB\r\n"
		+"  0\r\nTABLE\r\n  2\r\nVIEW\r\n 70\r\n0\r\n  0\r\nENDTAB\r\n"
		+"  0\r\nTABLE\r\n  2\r\nUCS\r\n 70\r\n0\r\n  0\r\nENDTAB\r\n" 
		+"  0\r\nTABLE\r\n  2\r\nAPPID\r\n 70\r\n1\r\n  0\r\nAPPID\r\n  2\r\nACAD\r\n 70\r\n0\r\n  0\r\nENDTAB\r\n"
		+"  0\r\nTABLE\r\n  2\r\nDIMSTYLE\r\n 70\r\n1\r\n  0\r\nDIMSTYLE\r\n  2\r\nStandard\r\n  3\r\n\r\n  4\r\n\r\n  5\r\n\r\n  6\r\n\r\n  7\r\n\r\n 40\r\n1.0\r\n 41\r\n2.5\r\n 42\r\n0.625\r\n 43\r\n3.75\r\n 44\r\n1.25\r\n 45\r\n0.0\r\n 46\r\n0.0\r\n 47\r\n0.0\r\n 48\r\n0.0\r\n 70\r\n0\r\n 71\r\n0\r\n 72\r\n0\r\n 73\r\n0\r\n 74\r\n0\r\n 75\r\n0\r\n 76\r\n0\r\n 77\r\n1\r\n 78\r\n8\r\n140\r\n2.5\r\n141\r\n2.5\r\n142\r\n0.0\r\n143\r\n0.03937007874016\r\n144\r\n1.0\r\n145\r\n0.0\r\n146\r\n1.0\r\n147\r\n0.625\r\n170\r\n0\r\n171\r\n3\r\n172\r\n1\r\n173\r\n0\r\n174\r\n0\r\n175\r\n0\r\n176\r\n0\r\n177\r\n0\r\n178\r\n0\r\n  0\r\nENDTAB\r\n"
		+"  0\r\nENDSEC\r\n"
		+"  0\r\nSECTION\r\n  2\r\nBLOCKS\r\n  0\r\nENDSEC\r\n"+"  0\r\nSECTION\r\n  2\r\nENTITIES\r\n";
		
		
		return header;
		
		
		
	}
	private Writer f;




	/**
	 * Creates an empty Dxf file with Header
	 * 
	 * @param filename
	 */
	DxfTools(String filename){


		try {
			 Log.d(TAG," Creating Dxf");
			 if (filename.equals(null)){
					filename="output.dxf";
					}
			 File path = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)+ "/dxfcamera/dxf/");
				path.mkdirs();
				File file = new File(path, filename);
			//File file = new File(Environment.getExternalStorageDirectory().getPath());
			
			this.filename=Uri.parse(file.getPath());


			FileOutputStream fout = new FileOutputStream (file);
			
					
			 f = new OutputStreamWriter(fout,"8859_1");
			 f.write(DxfHeader());


			 
			
		} catch (FileNotFoundException e) {

			Log.e(TAG,"DXFTools Constructor Call:",e);
		} catch (UnsupportedEncodingException e) {

			Log.e(TAG,"DXFTools Constructor Call:",e);
		} catch (IOException e) {

			Log.e(TAG,"DXFTools Constructor Call:",e);
		}
		
		
	}

	public Uri getFilename() {
		
		return filename;
	}




	/**
	 * 
	 * Add a drawing entity to the DXF file
	 * 
	 * @param dxfEntityString
	 */
	public void addentity(String dxfEntityString){
		try {
			 Log.d(TAG," adding entity");
			 if (dxfEntityString!=null){
			f.write(dxfEntityString);
			 }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(TAG,"addentity()",e);
		}
	}
	/**
	 * Closes DXF file
	 */
	public void close(){
		
		
		try {
			 Log.d(TAG," Closing DXF File");
			f.write("  0\r\nENDSEC\r\n  0\r\nEOF\r\n");
			f.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(TAG,"close()",e);
		}
	}
	
	
	
}
