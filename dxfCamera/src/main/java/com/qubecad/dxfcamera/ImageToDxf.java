package com.qubecad.dxfcamera;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.support.v4.content.FileProvider.getUriForFile;

public class ImageToDxf extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final String TAG = "ImageToDXF";
    private ImageView processableImage;
    private Bitmap sourceBitmap;
    private SeekBar seekBarImageNoise;
    private Activity activity;
    private Uri imageUri;
    private Uri filepathImage;
    private Mat mIntermediateMat;
    private Mat mRGB;
    private final String DATE_FORMAT_NOW = "ddHHmmss";
    private String layerColour="7";
    private String fileNamePrefix;
    private String capturedFilename = "";
    private SharedPreferences sharedPref;

    private boolean smoothingEnabled = false;
    private boolean usePoints = false;

    private static final int PERMISSIONS_REQUEST_WRITE_SDCARD = 1;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    protected void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_image_to_dxf);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        // Get intent, action and MIME type
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        activity = this;

        processableImage = (ImageView) findViewById(R.id.imageViewProcessableImage);
        seekBarImageNoise = (SeekBar) findViewById(R.id.seekBarImageNoise);

        if (Intent.ACTION_SEND.equals(action) && type != null) {


            if (intent != null) {

                try {

                    Display display = getWindowManager().getDefaultDisplay();
                    android.graphics.Point size = new android.graphics.Point();
                    display.getSize(size);
                    int width = size.x;
                    int height = size.y;


                    imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
                    InputStream stream = getContentResolver().openInputStream(imageUri);


                    sourceBitmap = BitmapHelper.decodeSampledBitmapFromStream(stream, width, height);
                    stream.close();

                    //Bitmap rgbaBitmap = sourceBitmap.copy(Bitmap.Config.ARGB_8888, true);

                    //processableImage.setImageBitmap(sourceBitmap);

                    processableImage.setImageBitmap(sourceBitmap);
                } catch (FileNotFoundException e) {
                    Log.e(TAG, "Error loading Intent data", e);
                } catch (IOException e) {
                    Log.e(TAG, "Error loading Intent data", e);
                }

            } else {
                finish();
            }


        } else {
            finish();
        }

        Log.i(TAG, action);

        seekBarImageNoise.setMax(500);
        seekBarImageNoise.setProgress(250);

        seekBarImageNoise.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                processableImage.setImageBitmap(processImage(sourceBitmap));


            }
        });


        ImageButton processButton = (ImageButton) findViewById(R.id.ProcessImageButton);

        processButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                // startCaptureImageTask();
                checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSIONS_REQUEST_WRITE_SDCARD);
            }

        });

        //ImageButton openButton = (ImageButton) findViewById(R.id.ProcessImageButton);

        //processButton.setOnClickListener(new View.OnClickListener() {

        //    public void onClick(View view) {
        //        openDxf(filepathImage);
        //    }

        //});


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void openDxf(Uri uri) {
        if (uri != null) {


            Log.d("Open Dxf", uri.toString());
            try {
                File sdDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getPath());

                Intent intent = new Intent();
                intent.setAction(android.content.Intent.ACTION_VIEW);


                intent.setDataAndType(uri, "image/vnd.dxf");
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);
            } catch (android.content.ActivityNotFoundException e) {
                Log.e(TAG, "", e);
            }

        } else {

        }

    }


    /**
     * @param intent
     * @return
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private String getData(Intent intent) {
        String filePath = "";
        if (intent.getData() != null) {
            filePath = intent.getData().getPath();
        } else if (intent.getClipData() != null) {
            ClipData.Item item = intent.getClipData().getItemAt(0);
            filePath = (String) item.getText();

        }
        return filePath;
    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "ImageToDxf Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.qubecad.dxfcamera/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    processableImage.setImageBitmap(processImage(sourceBitmap));

                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };


    private Bitmap processImage(Bitmap image) {
        mRGB = new Mat(image.getWidth(), image.getHeight(), CvType.CV_8UC2);

        Utils.bitmapToMat(image, mRGB);
        mIntermediateMat = new Mat(mRGB.cols(), mRGB.rows(), CvType.CV_8UC4);

        Imgproc.Canny(mRGB, mIntermediateMat, seekBarImageNoise.getProgress(), seekBarImageNoise.getProgress());


        return BitmapHelper.getBitmap(mIntermediateMat);

    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }


    /**
     * Set up and execute an Async Task to create the DXf file
     */
    private void startConvertToDxfFileTask() {

        // capture point

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        fileNamePrefix=sharedPref.getString(SettingsActivity.KEY_DXF_FILENAME_PREFIX,"Dxf_Camera_output");


        if (mIntermediateMat == null) {
            processImage(sourceBitmap);
        }

        if (mIntermediateMat.rows() > 0) {
            ConvertToDxfFileTask task = new ConvertToDxfFileTask();
            task.execute(mIntermediateMat);
        }

    }


    /**
     *
     */

    private class ConvertToDxfFileTask extends AsyncTask<Mat, Void, Uri> {
        private Bitmap finalBmp;


        protected Uri doInBackground(Mat... unit) {

            layerColour = sharedPref.getString(SettingsActivity.KEY_DXF_LINE_COLOUR, "7");
            fileNamePrefix = sharedPref.getString(SettingsActivity.KEY_DXF_FILENAME_PREFIX, "Dxf_Camera_output");
            smoothingEnabled = sharedPref.getBoolean(SettingsActivity.KEY_DXF_ENABLE_SMOOTHING, false);
            usePoints = sharedPref.getBoolean(SettingsActivity.KEY_DXF_USE_POINTS, false);


            String fileNameFront = fileNamePrefix +"-" +MiscUtils.now();
            String fileNameDXF = fileNameFront + ".dxf";
            String fileNameImage = fileNameFront + ".png";

            File path = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/dxfcamera/images/");
            path.mkdirs();
            File fileResultImage = new File(path, fileNameImage);

            filepathImage = getUriForFile(getBaseContext(), "com.qubecad.dxfcamera.fileprovider", fileResultImage);
            Log.i(TAG, fileResultImage.toString());

            finalBmp = getBitmap(unit[0]);

            List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
            Imgproc.findContours(unit[0], contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
            MatOfPoint approxf1 = new MatOfPoint();
            MatOfPoint2f points2f;
            MatOfPoint2f points2fOut;

            DxfTools dxf = new DxfTools(fileNameDXF);

            dxf.setLayerColour(layerColour);
            DxfEntity entity = null;
            Point[] vec=null;
            double epsilon;
            for (MatOfPoint mop : contours) {

                if (smoothingEnabled) {
                    points2f = new MatOfPoint2f(mop.toArray());
                    points2fOut = new MatOfPoint2f();
                    epsilon = 0.1 * Imgproc.arcLength(points2f, false);
                    Imgproc.approxPolyDP(points2f, points2fOut, epsilon, false);

                    points2fOut.convertTo(approxf1, CvType.CV_32S);

                    vec = approxf1.toArray();

                } else {
                    vec = mop.toArray();
                }

                if (usePoints) {
                    entity = new DxfPoints(vec, layerColour);
                } else {
                    entity = new PolyLine(vec, layerColour);
                }

                dxf.addentity(entity.getDxfEntityCode());
                entity = null;
                vec = null;

            }

            dxf.close();
            Uri resultFile = dxf.getFilename();

            MediaScannerConnection.scanFile(
                    getApplicationContext(),
                   new String[]{resultFile.getPath()} ,
                    null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            Log.d(TAG,"Scan media complete");
                        }
                    });

            dxf = null;

            return resultFile;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // create the dialog and attach it to the fragment manager
            ProgressDialogFragment dialog = new ProgressDialogFragment();

            dialog.show(getFragmentManager(), ProgressDialogFragment.PROGRESS_DIALOG_TAG);

        }

        protected void onPostExecute(Uri filename) {
            ProgressDialogFragment dialog = (ProgressDialogFragment) getFragmentManager()
                    .findFragmentByTag(ProgressDialogFragment.PROGRESS_DIALOG_TAG);
            if (dialog != null) {
                dialog.dismiss();
            }


            if (getIntent().getAction() != MediaStore.ACTION_IMAGE_CAPTURE) {
                IntentHelper.sendfile(filename.toString(), activity);
            } else {

                Uri saveUri = getIntent().getExtras().getParcelable(MediaStore.EXTRA_OUTPUT);

                if (saveUri != null) {
                    OutputStream imageOutputStream;
                    byte[] bmpByteArray = BitmapHelper.convertBitmaptoByteArray(finalBmp);

                    try {
                        imageOutputStream = getContentResolver().openOutputStream(saveUri);
                        imageOutputStream.write(bmpByteArray);
                        imageOutputStream.flush();
                        imageOutputStream.close();
                    } catch (FileNotFoundException e) {

                        e.printStackTrace();
                    } catch (IOException e) {

                        e.printStackTrace();
                    }
                }

                Intent resultIntent = new Intent();


                resultIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                resultIntent.setData(filepathImage);
                resultIntent.putExtra("dxffile", filename.toString());
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        }

    }

    private Bitmap getBitmap(Mat mat) {
        Bitmap bmp = null;
        Mat tmp = new Mat(mat.cols(), mat.width(), CvType.CV_8U, new Scalar(4));
        try {

            Imgproc.cvtColor(mat, tmp, Imgproc.COLOR_GRAY2RGBA, 4);
            bmp = Bitmap.createBitmap(tmp.cols(), tmp.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(tmp, bmp);
        } catch (CvException e) {
            Log.d("Exception", e.getMessage());
        }
        return bmp;
    }

    private void checkPermission(String permission, int callbackRequestId) {
        int permissionCheck = ContextCompat.checkSelfPermission(this, permission);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {

                // Explanation dialog goes here

                Toast.makeText(ImageToDxf.this, "Access required to allow application functionality", Toast.LENGTH_SHORT).show();

                ActivityCompat.requestPermissions(this, new String[]{permission}, callbackRequestId);

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this, new String[]{permission}, callbackRequestId);

                // The callback method gets the
                // result of the request.
            }
        } else {
            switch (callbackRequestId) {

                case PERMISSIONS_REQUEST_WRITE_SDCARD: {

                    startConvertToDxfFileTask();

                }
                return;

            }

        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mRGB != null) {
            mRGB.release();
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {


    }
}
