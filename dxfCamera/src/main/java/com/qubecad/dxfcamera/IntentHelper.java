package com.qubecad.dxfcamera;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;


import java.io.File;

import static android.support.v4.content.FileProvider.getUriForFile;

/**
 * Created by carl on 23/01/2016.
 */
public class IntentHelper {

    public static void sendfile(String filename,Activity activity) {

        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        // Mime type of the attachment (or) u can use sendIntent.setType("*/*")

        sendIntent.setType("application/x-dxf");


        // Full Path to the attachment
        File file=new File(filename);

        // Subject for the message or Email
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, file.getName());

        Uri contentUri = getUriForFile(activity, "com.qubecad.dxfcamera.fileprovider", file);

//        Uri contentUri =Uri.fromFile(file);

       sendIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION );

        sendIntent.putExtra(Intent.EXTRA_STREAM, contentUri);

        activity.startActivity(Intent.createChooser(sendIntent, "Send File..."));

    }
}
