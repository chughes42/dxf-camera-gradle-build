package com.qubecad.dxfcamera;

import org.opencv.core.Point;

import android.util.Log;

public class PolyLine implements DxfEntity {
	private int totalpoints=0;
	private Point[] points;
	private int Layer=0;
	private String colour;
	
	PolyLine(Point[] pointsin,String colour){
		this.points=pointsin;
		this.colour=colour;
	}
	

	public int getTotalpoints() {
		return totalpoints;
	}
	public void setTotalpoints(int totalpoints) {
		this.totalpoints = totalpoints;
	}
	public Point[] getPoints() {
		return points;
	}
	public void setPoints(Point[] points) {
		this.points = points;
	}
	public int getLayer() {
		return Layer;
	}
	public void setLayer(int layer) {
		Layer = layer;
	}
	
	public String getDxfEntityCode(){

		StringBuilder dxfEntityBuilder= new StringBuilder();
		dxfEntityBuilder.append("0\r\nPOLYLINE\r\n5\r\n31\r\n8\r\n0\r\n6\r\nBYLAYER\r\n62\r\n").append(colour).append("\r\n66\r\n1\r\n10\r\n0\r\n20\r\n0\r\n30\r\n0\r\n70\r\n0\r\n");



		 for (Point pnt :points ){
		//	 if (pointcount<=998){
			 //vertex= "0\r\nVERTEX\r\n5\r\n37\r\n8\r\n0\r\n6\r\nBYLAYER\r\n62\r\n256\r\n10\r\n"+String.valueOf(pnt.x)+"\r\n20\r\n-"+String.valueOf(pnt.y)+"\r\n30\r\n0\r\n";
			 dxfEntityBuilder.append("0\r\nVERTEX\r\n5\r\n37\r\n8\r\n0\r\n6\r\nBYLAYER\r\n62\r\n256\r\n10\r\n").append(pnt.x).append("\r\n20\r\n-").append(pnt.y).append("\r\n30\r\n0\r\n");
			// }
			// else{
			//	 Log.d("Polyline.class ","Maximum number of Vertexs reached.");
			// }
		 
		 
		 }


		dxfEntityBuilder.append("0\r\nSEQEND\r\n5\r\n35\r\n8\r\n0\r\n6\r\nBYLAYER\r\n62\r\n256\r\n");
		
				
		return dxfEntityBuilder.toString();
	}
	
	

}
