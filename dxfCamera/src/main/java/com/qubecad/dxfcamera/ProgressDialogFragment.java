package com.qubecad.dxfcamera;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;


public class ProgressDialogFragment extends DialogFragment {
public final static String PROGRESS_DIALOG_TAG = "ProgressDialog";
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		ProgressDialog dialog=new ProgressDialog(getActivity());
		dialog.setTitle("Exporting DXF");
		dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		
		
		return dialog;
	}
	
	

	

}
