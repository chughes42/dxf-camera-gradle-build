package com.qubecad.dxfcamera;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * Created by carl on 16/01/2017.
 */

public class SettingsActivity extends Activity {

    public static final String KEY_DXF_LINE_COLOUR="pref_dxfLineColour";
    public static final String KEY_DXF_LINE_WEIGHT="pref_dxfLineWeight";
    public static final String KEY_DXF_FILENAME_PREFIX="pref_dxf_filename_prefix";
    public static final String KEY_DXF_ENABLE_SMOOTHING ="pref_dxf_enable_smoothing" ;
    public static final String KEY_DXF_USE_POINTS = "pref_dxf_use_points";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }


}