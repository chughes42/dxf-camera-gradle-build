package com.qubecad.dxfcamera;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * Created by carl on 16/01/2017.
 */

public class SettingsFragment extends PreferenceFragment  {
    @Override
    public void onCreate(Bundle savedInstanceState) {

super.onCreate(savedInstanceState);
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.dxf_prefs);
    }


}